import { initTodos } from "../components/TodoSlice";
import { useDispatch } from "react-redux";
import { loadTodos } from "../apis/todo";

const useTodo = () => {
    const dispatch = useDispatch();

    const reloadTodos = () => {
        loadTodos().then((response) => {
            dispatch(initTodos(response.data))
        });
    }

    return {
        reloadTodos,
    }

}
export default useTodo;