import request from "./request";

export const loadTodos = () => {
    return request.get("/todos")
}

export const toggleTodo = (id, done) => {
    /*相当于done : done*/
    return request.put(`/todos/${id}`, {done})
}

export const createTodo = (todo) => {
    return request.post(`/todos/`, todo);
}

export const deleteTodo = (id) => {
    return request.delete(`/todos/${id}`);
}

export const editTodo = (id, text) => {
    return request.put(`/todos/${id}`, { text });
}
