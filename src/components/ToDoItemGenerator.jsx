import { useState } from "react";
import "./TodoGenerator.css"
import { createTodo } from "../apis/todo";
import useTodo from "../hooks/useTodo";
import { Button, Input } from "antd";

export const TodoItemGenerator = (props) => {
    const [content, setContent] = useState('');
    const [addLoading, setAddLoading] = useState(false)
    const { reloadTodos } = useTodo();

    const handleChangeInput = (e) => {
        setContent(e.target.value);
    }

    const handleChange = async () => {
        if (content.trim() !== '') {
            setAddLoading(true)
            await createTodo({
                id: new Date().toString(),
                text: content,
                done: false,
            })
            reloadTodos();
            setContent('');
            setAddLoading(false);
        }
    }

    const handleKepUp = (event) => {
        if (event.keyCode === 13) {
            handleChange()
        }
    }
    
    return (
        <div className="todo-generator">
            <Input type="text" value={content} onChange={handleChangeInput}
                onKeyUp={handleKepUp} placeholder="input a new todo here"
                className="input"></Input>
            <Button className="button" onClick={handleChange} loading={addLoading} disabled={addLoading}>add</Button>
        </div>
    )
}
