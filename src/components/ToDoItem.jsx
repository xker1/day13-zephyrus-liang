import "./TodoItem.css";
import { useNavigate } from "react-router-dom";
import deleteIcon from "../icon/delete.png";
// import { toggleTodo, deleteTodo } from "../apis/todo"
import * as api from "../apis/todo";
import useTodo from "../hooks/useTodo";
import { EditTwoTone } from "@ant-design/icons";
import { Modal, Input } from "antd";
import { useState } from "react";

export const TodoItem = (props) => {
  const navigate = useNavigate();
  const { reloadTodos } = useTodo();
  const text = props.value.text;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [editTodo, setEditTodo] = useState(text);

  const handleClick = async () => {
    if (props.canEdit) {
      await api.toggleTodo(props.value.id, !props.value.done);
      reloadTodos();
    } else {
      navigate(`/todo/${props.value.id}`);
    }
  };

  const deleteHandle = async () => {
    try {
        await api.deleteTodo(props.value.id);
        reloadTodos();
    } catch {}
  };

  const handleEditInput = (e) => {
    setEditTodo(e.target.value);
  }

  const showModal = () => {
    setIsModalOpen(true);
    setEditTodo(text);
  };

  const handleOk = async () => {
    await api.editTodo(props.value.id, editTodo);
    setIsModalOpen(false);
    reloadTodos();
    setEditTodo(text);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setEditTodo(text);
  };

  return (
    <div className="todo-item">
      <p
        className={
          props.value.done ? "todo-item-text-finish" : "todo-item-text"
        }
        onClick={handleClick}
      >
        {text}
      </p>
      <EditTwoTone className="itemEditIcon" onClick={showModal}/>
      <img
        src={deleteIcon}
        alt="delete-todo"
        className="itemDeleteIcon"
        onClick={deleteHandle}
      />
      <Modal open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <h4 className="modalTitle">Edit Todo</h4>
        <Input placeholder="Please enter your todo" value={editTodo} onChange={handleEditInput} className="modalInput"></Input>
      </Modal>
    </div>
  );
};
