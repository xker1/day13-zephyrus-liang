import { TodoGroup } from "./TodoGroup";
import { TodoItemGenerator } from "./ToDoItemGenerator";
import "./TodoList.css"
import { addTodo } from "./TodoSlice";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import useTodo from "../hooks/useTodo";
import "../fonts/font.css"

export const TodoList = () => {
    const todoList = useSelector(state => state.todo.todoList)
    const dispatch = useDispatch();
    const { reloadTodos } = useTodo();

    const handleUpdateTodoList = (newTodo) => {
        dispatch(addTodo(newTodo))
    }

    useEffect(() => {
        reloadTodos()
    }, [])

    return (
        <div className="todo-list">
            <h1 className="todo-list-title">Todo List</h1>
            <TodoGroup todoList={todoList} canEdit={true}></TodoGroup>
            <TodoItemGenerator onChange={handleUpdateTodoList}></TodoItemGenerator>
        </div>
    )
}
