import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
// import { useSearchParams } from "react-router-dom";

const TodoDetail = () => {
    const params = useParams();
    const navigator = useNavigate();
    
    const { id } = params;

    const todo = useSelector((state) => 
        state.todo.todoList.find(item => item.id === id)
    )

    useEffect(() => {
        if (!todo) {
            navigator("/404");
        }
    }, [todo]);

    return (
        <div>
            <h1>Detail</h1>
            { todo && (
                <div>
                    <p>{todo.text}</p>
                </div>
                )}
        </div>
    )
}
export default TodoDetail;